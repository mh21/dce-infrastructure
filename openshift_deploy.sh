#!/bin/bash

set -euo pipefail

function patch_images_resource() {
    if [ "$(shyaml get-value kind <<< "$1")" = "DeploymentConfig" ]; then
        patch_images_container "spec.template.spec.containers" "$1"
    else
        echo "$1"
    fi
}

function patch_images_container() {
    local container_key=$1 processed_resource=$2 container_index container_count original_image patched_image digest
    if [ "$(shyaml get-type "$container_key" <<< "$processed_resource")" = "sequence" ]; then
        container_count=$(shyaml get-length "$container_key" <<< "$processed_resource")
        for ((container_index=0; container_index < container_count; container_index++)); do
            original_image=$(shyaml get-value "$container_key.$container_index.image" <<< "$processed_resource")
            # replace tag by digest
            digest=$(skopeo inspect "docker://$original_image" | jq -r '.Digest' || true)
            if [ -n "$digest" ]; then
                echo "Patching $original_image -> $digest" >&2
                patched_image=${original_image/%[:@]*/@$digest}
                processed_resource=${processed_resource//$original_image/$patched_image}
            fi
        done
    fi
    echo "$processed_resource"
}

filename="resources/$PROJECT_NAME.yml"

case "${DEPLOY_TAG:-}" in
    staging) export DCE_NAMESPACE=dce-staging ;;
    mr-*) export DCE_NAMESPACE=dce-testing PROJECT_NAME=$PROJECT_NAME-$DEPLOY_TAG ;;
    *) export DCE_NAMESPACE=dce-production DEPLOY_TAG=production ;;
esac
oc project "$DCE_NAMESPACE"

if [ -v DEPLOY_DESTROY ]; then
    RESOURCES=secret,configmap,deploymentconfig,route,service
    oc delete "$RESOURCES" -l "app=$PROJECT_NAME"
else
    processed=$(envsubst < "$filename")
    # shellcheck disable=SC2001  # sed works just fine
    readarray -d '' processed_resources < <(sed 's/^---/\x00/' <<< "$processed")
    for processed_resource in "${processed_resources[@]}"; do
        if [[ "$(wc -w <<< "$processed_resource")" -eq 0 ]]; then continue; fi
        patch_images_resource "$processed_resource" | oc apply -f -
    done
fi

exit 0
